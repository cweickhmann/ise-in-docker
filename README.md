# ISE in Docker

## Preparation

First, [get Docker](https://docs.docker.com/engine/install/ubuntu/). More
specifically, get Docker Engine. I've linked the Ubuntu installation
instructions. There are instructions for other platforms, too. The following
will work independent of the host OS.

## Get ISE

Download the complete ISE installer tar file (named something like
`Xilinx_ISE_DS_Lin_14.7_1015_1.tar`).

## Build Directory 

Create an empty directory for all necessary files. I've called it
`docker-ise`. The name does not matter, though. Change into it.

Clone the repository into the directory:

```bash
user:~/docker-ise$ git clone https://gitlab.com/cweickhmann/ise-in-docker.git .
```
Mind the dot at the end!

Copy or link the installer file in the current directory:

```bash
# Either
user:~/docker-ise$ ln Downloads/Xilinx_ISE_DS_Lin_14.7_1015_1.tar
# Or
user:~/docker-ise$ mv Downloads/Xilinx_ISE_DS_Lin_14.7_1015_1.tar ./
```

Now you're ready to build the container.

## Build

```bash
docker build -t gui .
```

Depending on your internet connection this will run for a while.

## Run ISE

Once you're done, you can simply go to your directory and use the `run`
script:

```bash
user:~/docker-ise$ ./run
```


# Known Issues

## Starting Firefox from ISE

It seems, ISE comes with an old-ish version of `libstdc++6` so when trying
to open the website to get the free Webpack license, it fails with errors
like:

```
/usr/lib/firefox/firefox: /opt/Xilinx/14.7/ISE_DS/common//lib/lin64/libstdc++.so.6: version `GLIBCXX_3.4.10' not found (required by /usr/lib/firefox/firefox)
/usr/lib/firefox/firefox: /opt/Xilinx/14.7/ISE_DS/common//lib/lin64/libstdc++.so.6: version `GLIBCXX_3.4.11' not found (required by /usr/lib/firefox/firefox)
/usr/lib/firefox/firefox: /opt/Xilinx/14.7/ISE_DS/common//lib/lin64/libstdc++.so.6: version `GLIBCXX_3.4.9' not found (required by /usr/lib/firefox/firefox)
```

Not sure how to fix that, yet.

**Nota bene**: At least for the purpose of the license, ISE offers you to
save an HTML-File with the link to the license portal. I've downloaded a
Webpack license this way.

## Using docker-compose

A more accessible version of this would be to use `docker-compose` instead
of only a `Dockerfile`. WIP.
